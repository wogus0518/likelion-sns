package com.likelion.sns.repository;

import com.likelion.sns.domain.entity.LikeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikeRepository extends JpaRepository<LikeEntity, Long> {
    List<LikeEntity> findByPostId(Long postId);
}
