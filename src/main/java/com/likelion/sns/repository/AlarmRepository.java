package com.likelion.sns.repository;

import com.likelion.sns.domain.entity.Alarm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlarmRepository extends JpaRepository<Alarm, Long> {
    @Modifying
    @Query(value = "UPDATE Alarm set deleted = true where post.id = :id")
    void deleteAllByPostId(@Param("id") Long id);
}
