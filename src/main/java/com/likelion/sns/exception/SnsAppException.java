package com.likelion.sns.exception;

import lombok.Getter;

@Getter
public class SnsAppException extends RuntimeException {
    private final ErrorCode errorCode;
    private final String message;

    public SnsAppException(ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.message = errorCode.getMessage();
    }

    @Override
    public String toString() {
        return "UserException{" +
                "errorCode=" + errorCode +
                ", message='" + message + '\'' +
                '}';
    }
}
