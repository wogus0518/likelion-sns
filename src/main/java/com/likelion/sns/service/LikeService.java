package com.likelion.sns.service;

import com.likelion.sns.domain.AlarmType;
import com.likelion.sns.domain.entity.LikeEntity;
import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.repository.LikeRepository;
import com.likelion.sns.repository.PostRepository;
import com.likelion.sns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class LikeService {
    private final LikeRepository likeRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AlarmService alarmService;

    /**
     * 좋아요 추가
     * 좋아요 추가되었다고 알람 생성
     */
    @Transactional
    public void addLikes(Long postId, Authentication auth) {
        //포스트가 존재하는지 확인
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));
        //이미 좋아요를 눌렀는지 확인
        boolean alreadyLiked = post.alreadyLiked(auth.getName());
        if (alreadyLiked) {
            throw new SnsAppException(ErrorCode.ALREADY_LIKED);
        } else {
            //좋아요 누르기
            User user = userRepository.findByUserName(auth.getName()).get();
            LikeEntity likeEntity = LikeEntity.builder().user(user).post(post).build();
            likeRepository.save(likeEntity);

            //알람 생성
            alarmService.createAlarm(AlarmType.NEW_LIKE_ON_POST, post, auth);
        }
    }

    /**
     * 좋아요 개수 조회
     */
    public Integer countLikes(Long postId) {
        postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));
        return likeRepository.findByPostId(postId).size();
    }
}
