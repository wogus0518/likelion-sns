package com.likelion.sns.service;

import com.likelion.sns.domain.dto.user.UserDto;
import com.likelion.sns.domain.dto.user.UserJoinRequest;
import com.likelion.sns.domain.dto.user.UserLoginRequest;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.repository.UserRepository;
import com.likelion.sns.security.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.likelion.sns.exception.ErrorCode.INVALID_PASSWORD;
import static com.likelion.sns.exception.ErrorCode.USERNAME_NOT_FOUND;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String secretKey;
    private long expireTimeMs = 1000 * 60 * 60;

    @Transactional
    public UserDto join(UserJoinRequest userJoinRequest) {

        //userName 중복 체크
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent(user -> {
                    throw new SnsAppException(ErrorCode.DUPLICATED_LOGIN_ID);
                });

        //정상로직 - .save()
        User user = userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));
        return user.toDto();
    }

    public String login(UserLoginRequest userLoginRequest) {
        String token = "";
        String userName = userLoginRequest.getUserName();
        String password = userLoginRequest.getPassword();

        //userName 으로 검색해서 없으면 Not founded
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new SnsAppException(USERNAME_NOT_FOUND));

        //password 일치 여부 확인
        boolean matches = encoder.matches(password, user.getPassword());
        if(!matches) throw new SnsAppException(INVALID_PASSWORD);

        //정상로직 => 로그인 실행
        token = JwtTokenUtil.createToken(userName, secretKey, expireTimeMs);
        return token;
    }

    public User getUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new SnsAppException(USERNAME_NOT_FOUND));
    }
}
