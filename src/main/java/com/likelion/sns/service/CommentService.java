package com.likelion.sns.service;

import com.likelion.sns.domain.AlarmType;
import com.likelion.sns.domain.UserRole;
import com.likelion.sns.domain.dto.comment.CommentCreateRequest;
import com.likelion.sns.domain.dto.comment.CommentDto;
import com.likelion.sns.domain.dto.comment.CommentUpdateRequest;
import com.likelion.sns.domain.entity.Comment;
import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.repository.CommentRepository;
import com.likelion.sns.repository.PostRepository;
import com.likelion.sns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final AlarmService alarmService;

    /**
     * 댓글 생성
     */
    @Transactional
    public CommentDto saveComment(CommentCreateRequest createRequest, Authentication auth, Long postId) {
        //댓글을 달 게시글이 존재하는지
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));

        User user = userService.getUserByUserName(auth.getName());
        Comment comment = Comment.builder()
                .comment(createRequest.getComment())
                .user(user)
                .post(post)
                .build();
        //댓글 달기
        Comment savedComment = commentRepository.save(comment);

        //알람 추가하기
        alarmService.createAlarm(AlarmType.NEW_COMMENT_ON_POST, post, auth);

        return savedComment.toDto();
    }

    /**
     * 댓글 조회
     */
    public Page<Comment> getComments(Pageable pageable, Long postId) {
        //존재하는 게시글인지 확인
        postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));
        return commentRepository.findByPostId(pageable, postId);
    }

    /**
     * 댓글 수정
     */
    @Transactional
    public CommentDto doUpdate(Authentication auth, Long postId, Long commentId, CommentUpdateRequest commentUpdateRequest) {
        //게시글, 댓글이 모두 존재하는지 check
        Comment comment = isExist(postId, commentId);

        //수정 권한이 있는지 check
        if (hasPermission(comment, auth)) {
            Comment modifiedComment = comment.modify(commentUpdateRequest);
            return modifiedComment.toDto();
        } else {
            throw new SnsAppException(ErrorCode.INVALID_PERMISSION);
        }
    }

    /**
     * 댓글 삭제
     */
    @Transactional
    public Long doDelete(Authentication auth, Long postId, Long commentId) {
        //게시글, 댓글이 모두 존재하는지 check
        Comment comment = isExist(postId, commentId);

        //수정 권한이 있는지 check
        if (hasPermission(comment, auth)) {
            return comment.delete();
        } else {
            throw new SnsAppException(ErrorCode.INVALID_PERMISSION);
        }
    }

    private Comment isExist(Long postId, Long commentId) {
        postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.COMMENT_NOT_FOUND));
        return comment;
    }

    private static boolean hasPermission(Comment comment, Authentication auth) {
        List<String> roles = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        String userName = auth.getName();

        return roles.contains(UserRole.ROLE_ADMIN.toString()) || comment.getUser().getUsername().equals(userName);
    }

}
