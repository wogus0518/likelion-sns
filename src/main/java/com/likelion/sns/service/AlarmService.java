package com.likelion.sns.service;

import com.likelion.sns.domain.AlarmType;
import com.likelion.sns.domain.entity.Alarm;
import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.repository.AlarmRepository;
import com.likelion.sns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AlarmService {
    private final AlarmRepository alarmRepository;
    private final UserService userService;

    @Transactional
    public void createAlarm(AlarmType alarmType, Post post, Authentication auth) {
        User userByUserName = userService.getUserByUserName(auth.getName());
        Alarm alarm = Alarm.builder()
                .alarmType(alarmType)
                .user(post.getUser())
                .fromUserId(userByUserName.getId())
                .post(post)
                .text(alarmType.getDescription()).build();
        alarmRepository.save(alarm);
    }
}
