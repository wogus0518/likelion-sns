package com.likelion.sns.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.likelion.sns.domain.dto.post.PostDto;
import com.likelion.sns.domain.dto.post.PostUpdateRequest;
import com.likelion.sns.repository.AlarmRepository;
import com.likelion.sns.repository.CommentRepository;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SQLDelete(sql = "UPDATE post SET deleted = true where id = ?")
@Where(clause = "deleted = false")
public class Post extends BaseEntity {

    @Id @GeneratedValue
    @Column(name = "post_id")
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private List<LikeEntity> likes = new ArrayList<>();

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private List<Alarm> alarms = new ArrayList<>();

    private String title;
    private String body;

    private boolean deleted = Boolean.FALSE;


    /**
     * Post 수정 메서드
     */
    public Post modify(PostUpdateRequest request) {
        setTitle(request.getTitle());
        setBody(request.getBody());
        return this;
    }

    /**
     * Post 삭제 메서드
     */
    public void delete() {
        this.deleted = Boolean.TRUE;

    }

    /**
     *
     */
    public boolean alreadyLiked(String userName) {
        List<LikeEntity> likes = getLikes();
        return likes.stream()
                .anyMatch(likeEntity -> likeEntity.getUser().getUsername().equals(userName));
    }

    /**
     * DTO로 변환
     */
    public PostDto toDto() {
        return PostDto.builder()
                .id(this.id)
                .user(this.user)
                .title(this.title)
                .body(this.body)
                .createdAt(this.getCreatedAt())
                .lastModifiedAt(this.getLastModifiedAt())
                .build();
    }

    /**
     * Setter
     */
    private void setTitle(String title) {
        this.title = title;
    }

    private void setBody(String body) {
        this.body = body;
    }

}
