package com.likelion.sns.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.likelion.sns.domain.UserRole;
import com.likelion.sns.domain.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SQLDelete(sql = "UPDATE user SET deleted = true where id = ?")
@Where(clause = "deleted = false")
public class User extends BaseEntity implements UserDetails {

    @Id @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @Column(unique = true)
    private String userName;
    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    private boolean deleted = Boolean.FALSE;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<Post> posts = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<Comment> comments = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<LikeEntity> likes = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<Alarm> alarms = new ArrayList<>();

    public UserDto toDto() {
        return UserDto.builder()
                .id(this.id)
                .userName(this.userName)
                .password(this.password)
                .role(this.role)
                .build();
    }


    /**
     * 해당 유저의 권한 목록
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> auth = new ArrayList<>();
        auth.add(new SimpleGrantedAuthority(role.toString()));
        return auth;
    }

    /**
     * 계정의 고유한 값을 리턴 pk값
     */
    @Override
    public String getUsername() {
        return userName;
    }

    //계정 만료 여부 리턴 -> 만료:false
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //계정 잠김:false
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //false : 비밀번호 만료됨
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //true : 계정 활성화됨
    @Override
    public boolean isEnabled() {
        return true;
    }
}
