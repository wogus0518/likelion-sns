package com.likelion.sns.domain.entity;

import com.likelion.sns.domain.AlarmType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE alarm SET deleted = true where id = ?")
@Where(clause = "deleted = false")
public class Alarm extends BaseEntity{

    @Id @GeneratedValue
    @Column(name = "alarm_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private AlarmType alarmType;

    private boolean deleted = Boolean.FALSE;

    //알람을 받을 user
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    //댓글, 좋아요를 누른 사람
    private Long fromUserId;

    //댓글, 좋아요가 달린 포스트
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;

    //알람 내용
    private String text;
}
