package com.likelion.sns.domain;

public enum AlarmType {
    NEW_LIKE_ON_POST("new like!"),
    NEW_COMMENT_ON_POST("new comment!");

    private String description;

    AlarmType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
