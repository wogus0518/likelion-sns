package com.likelion.sns.domain.dto.post;

import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostUpdateRequest {
    private String title;
    private String body;

    public Post toEntity(User user) {
        return Post.builder()
                .user(user)
                .title(this.title)
                .body(this.body)
                .build();
    }
}
