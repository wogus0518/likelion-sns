package com.likelion.sns.domain.dto.alarm;

import com.likelion.sns.domain.AlarmType;
import com.likelion.sns.domain.entity.Alarm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlarmResponse {
//    private Long id;
//    private AlarmType alarmType;
//    private Long fromUserId;
//    private Long targetId;
//    private String text;
//    private LocalDateTime createdAt;
    private Page<Alarm> alarms;
}
