package com.likelion.sns.domain.dto.post;

import lombok.Getter;

@Getter
public class PostDeleteResponse {
    private final Long postId;
    private final String message = "포스트 삭제 완료";

    public PostDeleteResponse(Long postId) {
        this.postId = postId;
    }
}
