package com.likelion.sns.domain.dto.comment;

import com.likelion.sns.domain.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;

@Getter
@AllArgsConstructor
public class CommentListResponse {
    private final Page<Comment> posts;
}
