package com.likelion.sns.domain.dto.user;

import com.likelion.sns.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserJoinResponse {
    private final Long userId;
    private final String userName;
    private final UserRole role;
}
