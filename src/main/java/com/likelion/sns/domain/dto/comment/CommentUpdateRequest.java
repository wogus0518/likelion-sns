package com.likelion.sns.domain.dto.comment;

import lombok.*;

@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentUpdateRequest {
    private String comment;
}
