package com.likelion.sns.domain.dto.comment;

import lombok.*;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CommentCreateRequest {
    private String comment;
}
