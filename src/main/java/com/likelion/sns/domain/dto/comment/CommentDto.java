package com.likelion.sns.domain.dto.comment;

import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    private Long id;
    private String comment;
    private User user;
    private Post post;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;
}
