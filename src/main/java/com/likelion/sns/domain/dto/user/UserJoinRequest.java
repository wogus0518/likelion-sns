package com.likelion.sns.domain.dto.user;

import com.likelion.sns.domain.UserRole;
import com.likelion.sns.domain.entity.User;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserJoinRequest {
    private String userName;
    private String password;
    private UserRole role;

    public User toEntity(String password) {
        return User.builder()
                .userName(this.userName)
                .password(password)
                .role(this.role)
                .build();
    }

}
