package com.likelion.sns.domain.dto.user;

import com.likelion.sns.domain.UserRole;
import lombok.*;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;
    private String userName;
    private String password;
    private UserRole role;
}
