package com.likelion.sns.controller;

import com.likelion.sns.domain.Response;
import com.likelion.sns.domain.dto.alarm.AlarmResponse;
import com.likelion.sns.domain.entity.Alarm;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.service.AlarmService;
import com.likelion.sns.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/alarms")
@RequiredArgsConstructor
public class AlarmController {

    private final UserService userService;

    /**
     * 알람 조회
     */
    @GetMapping
    public Response<AlarmResponse> getAlarms(Authentication auth,
                                             @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "id") Pageable pageable) {
        User user = userService.getUserByUserName(auth.getName());
        List<Alarm> alarms = user.getAlarms();

        final Page<Alarm> page = getAlarmPage(pageable, alarms);

        return Response.success(new AlarmResponse(page));
    }

    /**
     * List<Alarm> -> Page<Alarm> 변환
     */
    private static Page<Alarm> getAlarmPage(Pageable pageable, List<Alarm> alarms) {
        final int start = (int) pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), alarms.size());

        return new PageImpl<>(alarms.subList(start, end), pageable, alarms.size());
    }
}
