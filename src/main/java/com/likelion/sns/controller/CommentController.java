package com.likelion.sns.controller;

import com.likelion.sns.domain.Response;
import com.likelion.sns.domain.dto.comment.*;
import com.likelion.sns.domain.entity.Comment;
import com.likelion.sns.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts/{postId}/comments")
public class CommentController {

    private final CommentService commentService;

    //== 댓글 작성==//
    @PostMapping
    public Response<CommentCreateResponse> createComment(Authentication auth, @PathVariable Long postId,
                                                         @RequestBody CommentCreateRequest commentCreateRequest) {
        CommentDto commentDto = commentService.saveComment(commentCreateRequest, auth, postId);
        return Response.success(new CommentCreateResponse(commentDto.getId(), commentDto.getComment(), commentDto.getUser().getUsername(),
                commentDto.getPost().getId(), commentDto.getCreatedAt(), commentDto.getLastModifiedAt()));
    }

    //== 댓글 조회==//
    @GetMapping
    public Response<CommentListResponse> getComments(@PathVariable Long postId,
                                                     @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        Page<Comment> comments = commentService.getComments(pageable, postId);
        return Response.success(new CommentListResponse(comments));
    }

    //== 댓글 수정 ==//
    @PutMapping("/{commentId}")
    public Response<CommentUpdateResponse> updateComment(Authentication auth,
                                                         @PathVariable Long commentId,
                                                         @PathVariable Long postId,
                                                         @RequestBody CommentUpdateRequest commentUpdateRequest) {
        CommentDto commentDto = commentService.doUpdate(auth, postId, commentId, commentUpdateRequest);
        return Response.success(new CommentUpdateResponse(commentDto.getId(), commentDto.getComment(), commentDto.getUser().getUsername(),
                commentDto.getPost().getId(), commentDto.getCreatedAt(), commentDto.getLastModifiedAt()));
    }

    //== 댓글 삭제==//
    @DeleteMapping("/{commentId}")
    public Response<CommentDeleteResponse> deleteComment(@PathVariable Long commentId,
                                                         @PathVariable Long postId,
                                                         Authentication auth) {
        Long deletedId = commentService.doDelete(auth, postId, commentId);
        return Response.success(new CommentDeleteResponse(deletedId, "댓글 삭제 완료"));
    }
}
