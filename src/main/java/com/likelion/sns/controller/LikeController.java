package com.likelion.sns.controller;

import com.likelion.sns.domain.Response;
import com.likelion.sns.service.LikeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts/{postId}/likes")
public class LikeController {
    private final LikeService likeService;

    //==좋아요 누르기==//
    @PostMapping
    public Response<String> addLike(@PathVariable Long postId,
                                    Authentication auth) {
        likeService.addLikes(postId, auth);
        return Response.success("좋아요를 눌렀습니다.");
    }

    //==좋아요 개수 조회==//
    @GetMapping
    public Response<Integer> countLikes(@PathVariable Long postId) {
        Integer count = likeService.countLikes(postId);
        return Response.success(count);
    }
}
