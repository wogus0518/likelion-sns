package com.likelion.sns.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.sns.domain.UserRole;
import com.likelion.sns.domain.dto.user.UserDto;
import com.likelion.sns.domain.dto.user.UserJoinRequest;
import com.likelion.sns.domain.dto.user.UserLoginRequest;
import com.likelion.sns.domain.entity.Alarm;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AlarmController.class)
class AlarmControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    @WithMockUser
    @DisplayName("알람 목록 조회 성공")
    void get_alarm_success() throws Exception {
        Alarm a = Alarm.builder().build();
        Alarm b = Alarm.builder().build();
        List<Alarm> alarmList = new ArrayList<>(Arrays.asList(a, b));
        User user = User.builder().alarms(alarmList).build();
        given(userService.getUserByUserName(any())).willReturn(user);

        ResultActions result = mockMvc.perform(get("/api/v1/alarms")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    @WithAnonymousUser
    @DisplayName("알람 목록 조회 실패 - 로그인하지 않은 경우")
    void get_alarm_fail() throws Exception {
        Alarm a = Alarm.builder().build();
        Alarm b = Alarm.builder().build();
        List<Alarm> alarmList = new ArrayList<>(Arrays.asList(a, b));
        User user = User.builder().alarms(alarmList).build();
        given(userService.getUserByUserName(any())).willReturn(user);

        ResultActions result = mockMvc.perform(get("/api/v1/alarms")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isUnauthorized())
                .andDo(print());
    }}