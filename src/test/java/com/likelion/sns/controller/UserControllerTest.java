package com.likelion.sns.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.sns.domain.UserRole;
import com.likelion.sns.domain.dto.user.UserDto;
import com.likelion.sns.domain.dto.user.UserJoinRequest;
import com.likelion.sns.domain.dto.user.UserLoginRequest;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    ObjectMapper objectMapper;

    //== Test에 필요한 변수 선언==//
    private final String USER_NAME = "testName";
    private final String PASSWORD = "password";
    private final UserRole USER_ROLE = UserRole.ROLE_USER;
    private final String TOKEN = "eyJhbGciOiJIUzI1NiJ9";

    private final UserDto USER_DTO = new UserDto(1L, USER_NAME, PASSWORD, USER_ROLE);
    private final UserLoginRequest LOGIN_REQUEST = new UserLoginRequest(USER_NAME, PASSWORD);
    private final UserJoinRequest JOIN_REQUEST = new UserJoinRequest(USER_NAME, PASSWORD, USER_ROLE);
    private final SnsAppException DUPLICATED_LOGIN_ID = new SnsAppException(ErrorCode.DUPLICATED_LOGIN_ID);
    private final SnsAppException USERNAME_NOT_FOUND = new SnsAppException(ErrorCode.USERNAME_NOT_FOUND);
    private final SnsAppException INVALID_PASSWORD = new SnsAppException(ErrorCode.INVALID_PASSWORD);

    @Test
    @WithMockUser
    @DisplayName("회원가입 성공")
    void join_success() throws Exception {

        given(userService.join(any(UserJoinRequest.class))).willReturn(USER_DTO);

        ResultActions result = mockMvc.perform(post("/api/v1/users/join")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(JOIN_REQUEST)));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.userId").exists())
                .andExpect(jsonPath("$.result.userName").value(USER_NAME))
                .andExpect(jsonPath("$.result.password").doesNotExist())
                .andExpect(jsonPath("$.result.role").value(USER_ROLE.name()))
                .andDo(print());

        verify(userService).join(any(UserJoinRequest.class));
    }

    @Test
    @WithMockUser
    @DisplayName("회원가입 실패 - userName 중복")
    void join_fail_duplicated_userName() throws Exception {

        when(userService.join(any(UserJoinRequest.class))).thenThrow(DUPLICATED_LOGIN_ID);

        ResultActions result = mockMvc.perform(post("/api/v1/users/join")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(JOIN_REQUEST)));

        result.andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("DUPLICATED_LOGIN_ID"))
                .andExpect(jsonPath("$.result.message").value("UserName이 중복됩니다."))
                .andDo(print());

        verify(userService).join(any(UserJoinRequest.class));
    }

    @Test
    @WithMockUser
    @DisplayName("로그인 성공")
    void login_success() throws Exception{

        given(userService.login(any(UserLoginRequest.class))).willReturn(TOKEN);

        ResultActions result = mockMvc.perform(post("/api/v1/users/login")
                .with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(LOGIN_REQUEST)));
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.jwt").value(TOKEN))
                .andDo(print());

        verify(userService).login(any(UserLoginRequest.class));
    }

    @Test
    @WithMockUser
    @DisplayName("로그인 실패 - userName없음")
    void login_fail_not_found_userName() throws Exception{

        when(userService.login(any(UserLoginRequest.class))).thenThrow(USERNAME_NOT_FOUND);

        ResultActions result = mockMvc.perform(post("/api/v1/users/login")
                .with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(LOGIN_REQUEST)));

        result.andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("USERNAME_NOT_FOUND"))
                .andExpect(jsonPath("$.result.message").value("Not founded"))
                .andDo(print());

        verify(userService).login(any(UserLoginRequest.class));
    }

    @Test
    @WithMockUser
    @DisplayName("로그인 실패 - password틀림")
    void login_fail_wrong_password() throws Exception{

        when(userService.login(any(UserLoginRequest.class))).thenThrow(INVALID_PASSWORD);

        ResultActions result = mockMvc.perform(post("/api/v1/users/login")
                .with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(LOGIN_REQUEST)));

        result.andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("INVALID_PASSWORD"))
                .andExpect(jsonPath("$.result.message").value("패스워드가 잘못되었습니다."))
                .andDo(print());

        verify(userService).login(any(UserLoginRequest.class));
    }
}