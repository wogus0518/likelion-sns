package com.likelion.sns.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.sns.domain.dto.post.PostCreateRequest;
import com.likelion.sns.domain.dto.post.PostDto;
import com.likelion.sns.domain.dto.post.PostUpdateRequest;
import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.security.entrypoint.CustomAuthenticationEntryPoint;
import com.likelion.sns.service.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostController.class)
@Import(CustomAuthenticationEntryPoint.class)
public class PostControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    PostService postService;

    //======== 테스트 필요 변수 선언 ========//
    private final Long POST_ID = 1L;
    private final String USER_NAME = "jaehyun";
    private final String TITLE = "This is title";
    private final String BODY = "This is body";

    private final PostCreateRequest CREATE_REQUEST = new PostCreateRequest(TITLE, BODY);
    private final PostUpdateRequest UPDATE_REQUEST = new PostUpdateRequest(TITLE, BODY);
    private final User USER = User.builder().userName(USER_NAME).build();
    private final PostDto POST_DTO = new PostDto(POST_ID, USER, TITLE, BODY, null, null);

    private final SnsAppException INVALID_PERMISSION = new SnsAppException(ErrorCode.INVALID_PERMISSION);
    private final SnsAppException DATABASE_ERROR = new SnsAppException(ErrorCode.DATABASE_ERROR);

    //======== 포스트 작성 ========//
    @Test
    @WithMockUser(value = "test")
    @DisplayName("포스트 작성 성공")
    void create_success() throws Exception {
        given(postService.savePost(any(PostCreateRequest.class), any(String.class))).willReturn(POST_DTO);

        ResultActions result = mockMvc.perform(post("/api/v1/posts")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(CREATE_REQUEST)));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(POST_ID))
                .andExpect(jsonPath("$.result.message").value("포스트 등록 완료"))
                .andDo(print());
    }

    @Test
    @DisplayName("포스트 작성 실패(1) - 인증 실패 - JWT를 Bearer Token으로 보내지 않은 경우")
    void create_fail_token_not_start_with_bearer() throws Exception {

    }

    @Test
    @DisplayName("포스트 작성 실패(2) - 인증 실패 - JWT가 유효하지 않은 경우")
    void create_fail_jwt_invalid() throws Exception {

    }

    //======== 포스트 수정 ========//
    @Test
    @WithAnonymousUser
    @DisplayName("포스트 수정 실패(1) : 인증 실패")
    void update_fail_authentication_fail() throws Exception {
        given(postService.doUpdate(any(), any(), any())).willThrow(INVALID_PERMISSION);

        ResultActions result = mockMvc.perform(put("/api/v1/posts/{postId}", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(UPDATE_REQUEST)));

        result.andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason("Unauthorized"));
    }

    @Test
    @WithMockUser
    @DisplayName("포스트 수정 실패(2) : 작성자 불일치")
    void update_fail_unmatched_writer() throws Exception {
        given(postService.doUpdate(any(), any(), any())).willThrow(INVALID_PERMISSION);

        ResultActions result = mockMvc.perform(put("/api/v1/posts/{postId}", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(UPDATE_REQUEST)));

        result.andDo(print())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("INVALID_PERMISSION"))
                .andExpect(jsonPath("$.result.message").value("사용자가 권한이 없습니다."))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser
    @DisplayName("포스트 수정 실패(3) : 데이터베이스 에러")
    void update_fail_db_error() throws Exception {
        given(postService.doUpdate(any(), any(), any())).willThrow(DATABASE_ERROR);

        ResultActions result = mockMvc.perform(put("/api/v1/posts/{postId}", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(UPDATE_REQUEST)));

        result.andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("DATABASE_ERROR"))
                .andExpect(jsonPath("$.result.message").value("DB에러"));
    }

    @Test
    @WithMockUser
    @DisplayName("포스트 수정 성공(1) : 작성자가 수정 요청")
    void update_success_writer_request() throws Exception {
        when(postService.doUpdate(any(), any(), any())).thenReturn(POST_DTO);

        ResultActions result = mockMvc.perform(put("/api/v1/posts/{postId}", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(UPDATE_REQUEST)));

        result.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(POST_ID))
                .andExpect(jsonPath("$.result.message").value("포스트 수정 완료"));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    @DisplayName("포스트 수정 성공(2) : 관리자가 수정 요청")
    void update_success_admin_request() throws Exception {
        when(postService.doUpdate(any(), any(), any())).thenReturn(POST_DTO);

        ResultActions result = mockMvc.perform(put("/api/v1/posts/{postId}", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(UPDATE_REQUEST)));

        result.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(POST_ID))
                .andExpect(jsonPath("$.result.message").value("포스트 수정 완료"));
    }

    //======== 포스트 삭제 ========//
    @Test
    @WithMockUser
    @DisplayName("포스트 삭제 성공(1) : 작성자가 삭제 요청")
    void delete_success_writer_request() throws Exception {
        when(postService.doDelete(any(), any())).thenReturn(POST_ID);

        ResultActions result = mockMvc.perform(delete("/api/v1/posts/{postId}", POST_ID)
                .with(csrf()));

        result.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(POST_ID))
                .andExpect(jsonPath("$.result.message").value("포스트 삭제 완료"));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    @DisplayName("포스트 삭제 성공(2) : 관리자가 삭제 요청")
    void delete_success_admin_request() throws Exception {
        when(postService.doDelete(any(), any())).thenReturn(POST_ID);

        ResultActions result = mockMvc.perform(delete("/api/v1/posts/{postId}", POST_ID)
                .with(csrf()));

        result.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(POST_ID))
                .andExpect(jsonPath("$.result.message").value("포스트 삭제 완료"));
    }

    @Test
    @WithAnonymousUser
    @DisplayName("포스트 수정 실패(1) : 인증 실패")
    void delete_fail_authentication_fail() throws Exception {
        given(postService.doDelete(any(), any())).willThrow(INVALID_PERMISSION);

        ResultActions result = mockMvc.perform(delete("/api/v1/posts/{postId}", POST_ID)
                .with(csrf()));

        result.andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason("Unauthorized"));
    }

    @Test
    @WithMockUser
    @DisplayName("포스트 수정 실패(2) : 작성자 불일치")
    void delete_fail_unmatched_writer() throws Exception {
        given(postService.doDelete(any(), any())).willThrow(INVALID_PERMISSION);

        ResultActions result = mockMvc.perform(delete("/api/v1/posts/{postId}", POST_ID)
                .with(csrf()));

        result.andDo(print())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("INVALID_PERMISSION"))
                .andExpect(jsonPath("$.result.message").value("사용자가 권한이 없습니다."))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser
    @DisplayName("포스트 수정 실패(3) : 데이터베이스 에러")
    void delete_fail_db_error() throws Exception {
        given(postService.doDelete(any(), any())).willThrow(DATABASE_ERROR);

        ResultActions result = mockMvc.perform(delete("/api/v1/posts/{postId}", POST_ID)
                .with(csrf()));

        result.andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("DATABASE_ERROR"))
                .andExpect(jsonPath("$.result.message").value("DB에러"));
    }

    //======== 포스트 조회 ========//
    @Test
    @WithMockUser
    @DisplayName("상세 조회 성공 - id, title, body, userName 4가지 항목이 있는지 검증")
    void select_one_success() throws Exception {
        when(postService.findById(any())).thenReturn(POST_DTO);

        ResultActions result = mockMvc.perform(get("/api/v1/posts/{postId}", POST_ID)
                .with(csrf()));

        result.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(POST_ID))
                .andExpect(jsonPath("$.result.title").value(TITLE))
                .andExpect(jsonPath("$.result.body").value(BODY))
                .andExpect(jsonPath("$.result.userName").value(USER.getUsername()));
    }

    @Test
    @DisplayName("조회 성공 : 0번이 1번보다 날짜가 최신")
    void select_all_success() {

    }

    //== 마이피드 조회 ==//
    @Test
    @WithMockUser
    @DisplayName("마이피드 조회 성공")
    void get_myFeed_success() throws Exception {
        when(postService.findMyFeed(any(), any())).thenReturn(Page.empty());

        mockMvc.perform(get("/api/v1/posts/my")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    @DisplayName("마이피드 조회 실패(1) - 로그인 하지 않은 경우")
    void get_myFeed_fail() throws Exception {
        when(postService.findMyFeed(any(), any())).thenReturn(Page.empty());

        mockMvc.perform(get("/api/v1/posts/my")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}
