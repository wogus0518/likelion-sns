package com.likelion.sns.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.sns.domain.dto.comment.*;
import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.security.entrypoint.CustomAuthenticationEntryPoint;
import com.likelion.sns.service.CommentService;
import com.likelion.sns.service.LikeService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LikeController.class)
@Import(CustomAuthenticationEntryPoint.class)
public class LikeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    LikeService likeService;

    @MockBean
    CommentService commentService;

    //======== 테스트 필요 변수 선언 ========//
    private final Long COMMENT_ID = 1L;
    private final Long POST_ID = 1L;
    private final String USER_NAME = "jaehyun";
    private final String COMMENT = "This is comment";

    private final CommentCreateRequest CREATE_REQUEST = new CommentCreateRequest(COMMENT);
    private final CommentCreateResponse CREATE_RESPONSE = CommentCreateResponse.builder().build();
    private final CommentUpdateRequest UPDATE_REQUEST = new CommentUpdateRequest(COMMENT);
    private final CommentUpdateResponse UPDATE_RESPONSE = new CommentUpdateResponse();
    private final User USER = User.builder().userName(USER_NAME).build();
    private final Post POST = Post.builder().build();
    private final CommentDto COMMENT_DTO = new CommentDto(COMMENT_ID, COMMENT, USER, POST, null, null);

    //== 좋아요 누르기 ==//
    @Test
    @WithMockUser
    @DisplayName("좋아요 누르기 성공")
    void press_likes_success() throws Exception {
        doNothing().when(likeService).addLikes(any(), any());

        ResultActions result = mockMvc.perform(post("/api/v1/posts/{postId}/likes", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result").value("좋아요를 눌렀습니다."))
                .andDo(print());
    }

    @Test
    @WithAnonymousUser
    @DisplayName("좋아요 누르기 실패(1) - 로그인 하지 않은 경우")
    void press_likes_fail_unauthorized() throws Exception {
        doNothing().when(likeService).addLikes(any(), any());

        ResultActions result = mockMvc.perform(post("/api/v1/posts/{postId}/likes", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isUnauthorized())
                .andDo(print());
    }

    @Test
    @WithMockUser
    @DisplayName("좋아요 누르기 실패(2) - 해당 Post가 없는 경우")
    void press_likes_fail_no_post() throws Exception {
        doThrow(new SnsAppException(ErrorCode.POST_NOT_FOUND)).when(likeService).addLikes(any(), any());

        ResultActions result = mockMvc.perform(post("/api/v1/posts/{postId}/likes", POST_ID)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value("POST_NOT_FOUND"))
                .andExpect(jsonPath("$.result.message").value("해당 포스트가 없습니다."))
                .andDo(print());
    }

    //==댓글 수정==//
    @Test
    @WithMockUser
    @DisplayName("댓글 수정 성공")
    void update_comment_success() throws Exception {
        when(commentService.doUpdate(any(), any(), any(), any())).thenReturn(COMMENT_DTO);

        mockMvc.perform(put("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UPDATE_REQUEST)))
                .andDo(print())
                .andExpect(status().isOk());

        verify(commentService).doUpdate(any(), any(), any(), any());
    }

    @Test
    @WithAnonymousUser
    @DisplayName("댓글 수정 실패(1) - 인증 실패")
    void update_comment_fail_unauth() throws Exception {
        when(commentService.doUpdate(any(), any(), any(), any())).thenReturn(COMMENT_DTO);

        mockMvc.perform(put("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UPDATE_REQUEST)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 수정 실패(2) - Post 없는 경우")
    void update_comment_fail_not_exist_post() throws Exception {
        when(commentService.doUpdate(any(), any(), any(), any())).thenThrow(new SnsAppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(put("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UPDATE_REQUEST)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.POST_NOT_FOUND.toString()));

        verify(commentService).doUpdate(any(), any(), any(), any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 수정 실패(3) - 작성자 불일치")
    void update_comment_fail_unmatched_writer() throws Exception {
        when(commentService.doUpdate(any(), any(), any(), any())).thenThrow(new SnsAppException(ErrorCode.INVALID_PERMISSION));

        mockMvc.perform(put("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UPDATE_REQUEST)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.INVALID_PERMISSION.toString()));

        verify(commentService).doUpdate(any(), any(), any(), any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 수정 실패(4) - 데이터베이스 에러")
    void update_comment_fail_database_error() throws Exception {
        when(commentService.doUpdate(any(), any(), any(), any())).thenThrow(new SnsAppException(ErrorCode.DATABASE_ERROR));

        mockMvc.perform(put("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UPDATE_REQUEST)))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.DATABASE_ERROR.toString()));

        verify(commentService).doUpdate(any(), any(), any(), any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 삭제 성공")
    void delete_comment_success() throws Exception {
        when(commentService.doDelete(any(), any(), any())).thenReturn(COMMENT_ID);

        mockMvc.perform(delete("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.message").value("댓글 삭제 완료"));

        verify(commentService).doDelete(any(), any(), any());
    }

    @Test
    @WithAnonymousUser
    @DisplayName("댓글 삭제 실패(1) : 인증 실패")
    void delete_comment_fail_unauthorized() throws Exception {
        when(commentService.doDelete(any(), any(), any())).thenReturn(COMMENT_ID);

        mockMvc.perform(delete("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 삭제 실패(2) : Post없는 경우")
    void delete_comment_fail_no_post() throws Exception {
        when(commentService.doDelete(any(), any(), any())).thenThrow(new SnsAppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(delete("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.POST_NOT_FOUND.toString()));

        verify(commentService).doDelete(any(), any(), any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 삭제 실패(3) : 작성자 불일치")
    void delete_comment_fail_unmatched_writer() throws Exception {
        when(commentService.doDelete(any(), any(), any())).thenThrow(new SnsAppException(ErrorCode.INVALID_PERMISSION));

        mockMvc.perform(delete("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.INVALID_PERMISSION.toString()));

        verify(commentService).doDelete(any(), any(), any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 삭제 실패(4) : 데이터베이스 에러")
    void delete_comment_fail_db_error() throws Exception {
        when(commentService.doDelete(any(), any(), any())).thenThrow(new SnsAppException(ErrorCode.DATABASE_ERROR));

        mockMvc.perform(delete("/api/v1/posts/{postId}/comments/{commentId}", POST_ID, COMMENT_ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.DATABASE_ERROR.toString()));

        verify(commentService).doDelete(any(), any(), any());
    }
}
