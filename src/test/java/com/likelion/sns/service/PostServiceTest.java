package com.likelion.sns.service;

import com.likelion.sns.domain.dto.post.PostCreateRequest;
import com.likelion.sns.domain.dto.post.PostDto;
import com.likelion.sns.domain.dto.post.PostUpdateRequest;
import com.likelion.sns.domain.entity.Post;
import com.likelion.sns.domain.entity.User;
import com.likelion.sns.exception.ErrorCode;
import com.likelion.sns.exception.SnsAppException;
import com.likelion.sns.repository.AlarmRepository;
import com.likelion.sns.repository.CommentRepository;
import com.likelion.sns.repository.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(PostService.class)
public class PostServiceTest {

    @MockBean
    PostRepository postRepository;

    @MockBean
    UserService userService;

    @MockBean
    CommentRepository commentRepository;

    @MockBean
    AlarmRepository alarmRepository;

    @Autowired
    MockMvc mockMvc;

    private PostService postService;

    @BeforeEach
    void setUpTest() {
        postService = new PostService(postRepository, commentRepository, alarmRepository, userService);
    }

    Long POST_ID = 1L;
    PostCreateRequest CREATE_REQUEST = new PostCreateRequest("title", "body");
    PostUpdateRequest UPDATE_REQUEST = new PostUpdateRequest("title2", "body2");
    User USER = User.builder()
            .userName("writer")
            .password("password")
            .build();
    Post POST = Post.builder()
            .title(CREATE_REQUEST.getTitle())
            .body(CREATE_REQUEST.getBody())
            .user(USER)
            .build();

    //== 포스트 등록 ==//
    @Test
    @DisplayName("등록 성공")
    void create_success() {
        when(userService.getUserByUserName(any())).thenReturn(USER);
        when(postRepository.save(any())).thenReturn(POST);
        PostDto postDto = postService.savePost(CREATE_REQUEST, USER.getUsername());

        assertEquals(postDto.getTitle(), CREATE_REQUEST.getTitle());
        assertEquals(postDto.getBody(), CREATE_REQUEST.getBody());
    }

    @Test
    @DisplayName("등록 실패 : 유저가 존재하지 않을 때")
    void create_fail_no_user() {
        when(userService.getUserByUserName(USER.getUsername())).thenThrow(new SnsAppException(ErrorCode.USERNAME_NOT_FOUND));

        SnsAppException snsAppException = assertThrowsExactly(SnsAppException.class, () -> {
            postService.savePost(CREATE_REQUEST, USER.getUsername());
        });
        assertEquals(snsAppException.getErrorCode(), ErrorCode.USERNAME_NOT_FOUND);
    }

    //== 포스트 수정 ==//
    @Test
    @WithMockUser
    @DisplayName("수정 실패 : 포스트 존재하지 않음")
    void update_fail_post_not_founded() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        when(postRepository.findById(any())).thenThrow(new SnsAppException(ErrorCode.POST_NOT_FOUND));

        SnsAppException snsAppException = assertThrowsExactly(SnsAppException.class, () -> {
            postService.doUpdate(POST_ID, UPDATE_REQUEST, authentication);
        });
        assertEquals(snsAppException.getErrorCode(), ErrorCode.POST_NOT_FOUND);
    }

    @Test
    @WithMockUser(value = "other", roles = {"USER"})
    @DisplayName("수정 실패 : 작성자!=유저")
    void update_fail_unmatched_writer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        //여기서 반환되는 Post의 작성자는 "writer"
        when(postRepository.findById(any())).thenReturn(Optional.of(POST));

        SnsAppException snsAppException = assertThrowsExactly(SnsAppException.class, () -> {
            //실제 업데이트를 요청하는 사람은 "other"
            postService.doUpdate(POST_ID, UPDATE_REQUEST, authentication);
        });
        assertEquals(snsAppException.getErrorCode(), ErrorCode.INVALID_PERMISSION);
    }

    @Test
    @WithMockUser
    @DisplayName("수정 실패 : 유저 존재하지 않음")
    void update_fail_user_not_founded() {

    }

    @Test
    @WithMockUser(value = "other", roles = {"ADMIN"})
    @DisplayName("수정 성공 : 작성자!=유저 이지만 ADMIN인 경우")
    void update_success_admin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        when(postRepository.findById(POST_ID)).thenReturn(Optional.of(POST));
        PostDto updatedPost = postService.doUpdate(POST_ID, UPDATE_REQUEST, authentication);

        assertEquals(updatedPost.getTitle(), UPDATE_REQUEST.getTitle());
    }

    @Test
    @WithMockUser(value = "writer", roles = {"USER"})
    @DisplayName("수정 성공 : 작성자 요청")
    void update_success_writer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        when(postRepository.findById(POST_ID)).thenReturn(Optional.of(POST));
        PostDto updatedPost = postService.doUpdate(POST_ID, UPDATE_REQUEST, authentication);

        assertEquals(updatedPost.getTitle(), UPDATE_REQUEST.getTitle());
    }

    //== 포스트 삭제 ==//
    @Test
    @WithMockUser
    @DisplayName("삭제 실패 : 포스트 존재하지 않음")
    void delete_fail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        when(postRepository.findById(any())).thenThrow(new SnsAppException(ErrorCode.POST_NOT_FOUND));
        SnsAppException snsAppException = assertThrowsExactly(SnsAppException.class, () -> {
            postService.doDelete(POST_ID, authentication);
        });
        assertEquals(snsAppException.getErrorCode(), ErrorCode.POST_NOT_FOUND);
    }

    @Test
    @WithMockUser(value = "other")
    @DisplayName("삭제 실패 : 작성자!=유저")
    void delete_fail_unmatched_writer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        //삭제할 post의 작성자는 "writer"
        when(postRepository.findById(POST_ID)).thenReturn(Optional.of(POST));

        SnsAppException snsAppException = assertThrowsExactly(SnsAppException.class, () -> {
            //삭제를 요청하는 사람은 "other"
            postService.doDelete(POST_ID, authentication);
        });
        assertEquals(snsAppException.getErrorCode(), ErrorCode.INVALID_PERMISSION);
    }

    @Test
    @WithMockUser(value = "other", roles = {"ADMIN"})
    @DisplayName("삭제 성공 : ADMIN인 경우")
    void delete_success_admin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        when(postRepository.findById(POST_ID)).thenReturn(Optional.of(POST));
        Long deletedId = postService.doDelete(POST_ID, authentication);

        assertEquals(deletedId, POST.getId());
    }

}
